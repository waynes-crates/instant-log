#![doc = r"# Instant Log

This incredibly simple log with a timestamp, log level and a log message
Console only, file options may be added but this is really intended for containers."]
use chrono::Local;
use dotenv::dotenv;


fn log (level: &str, message: String) {
    dotenv().ok();
    let log_level = std::env::var("LOG_LEVEL").unwrap_or("INFO".to_string());
    let config_level = level_number_from_string(log_level.as_str());
    let passed_level = level_number_from_string(level);

    // No logging if the config level is higher than the passed level
    if passed_level > config_level {
        return;
    }

    let now = Local::now();
    let timestamp = now.format("%Y-%m-%d %H:%M:%S").to_string();
    let level = level.to_uppercase();
    if level == "FATAL" {
        eprintln!("{} - {} - {}", timestamp, level, message);
        std::process::exit(1);
    } else if level == "ERROR" {
        eprintln!("{} - {} - {}", timestamp, level, message);
    } else {
        println!("{} - {} - {}", timestamp, level, message);
    }
}

fn level_number_from_string(log_level: &str) -> u8 {
    match log_level {
        "TRACE"|"trace" => 6,
        "DEBUG"|"debug" => 5,
        "INFO"|"info" => 4,
        "WARN"|"warn" => 3,
        "ERROR"|"error" => 2,
        "FATAL"|"fatal" => 1,
        _ => 0
    }
}

/// Logs a message to stdout
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// let sql = "SELECT * FROM table";
/// log::trace(sql);
/// 
/// ```
/// 
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - TRACE - SELECT * FROM table
/// ```
pub fn trace (message: String) {
    log("TRACE", message);
}

/// Logs a message to stdout
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// log::debug("debug message");
/// 
/// ```
/// 
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - DEBUG - debug message
/// ```
pub fn debug (message: String) {
    log("DEBUG", message);
}

/// Logs a message to stdout
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// log::info("Information message");
/// 
/// ```
/// 
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - INFO  - Information message
/// ```
pub fn info (message: String) {
    log("INFO ", message);
}

/// Logs a message to stdout
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// log::warn("Warning message");
/// 
/// ```
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - WARN  - Warning message
/// ```
pub fn warn (message: String) {
    log("WARN ", message);
}

/// Logs a message to stderr
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// log::error("No rows returned from database");
/// 
/// ```
/// 
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - ERROR - No rows returned from database
/// ```
pub fn error (message: String) {
    log("ERROR", message);
}

/// Logs a message to stderr and exits with code 1
/// 
/// # Example
/// ```
/// use instant_log as log;
/// 
/// log::fatal("Database connection failed".to_string());
/// ```
/// 
/// # Output
/// ```text
/// 2021-01-01 00:00:00 - FATAL - Database connection failed
/// ```
pub fn fatal (message: String) {
    log("FATAL", message);
}
