# Instant Log

I really like logs in a very simple consistent way across my applications.
This incredibly simple log with a timestamp, log level and a log message
Console only, file options may be added but this is really intended for containers.

## Example Output

```
2023-06-18 14:26:00 - INFO  - Hello World!
2023-06-18 14:26:10 - ERROR - Bye World!
```

## Example Usage

```rust
use instant_log as log;

fn main() {
    log::debug(format("Hello {}!", "World"));
    log::info("Hello World!");
    log::warn("Be careful!");
    log::error("Too Late!");
    log::fatal(format("error {}!", error.to_string()));
}
```

Configuration can be done via environment variable `LOG_LEVEL` 
Valid log levels, defaults to `INFO`

- debug|DEBUG
- info|INFO
- warn|WARN
- error|ERROR
- fatal|FATAL
